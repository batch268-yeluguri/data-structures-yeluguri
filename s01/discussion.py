# [SECTION] Objects in Python
# Everything in Python is an object 

# int 
print(f'Integer data type: {isinstance(int, object)}')

# str
print(f'String data type: {isinstance(str, object)}')

#integer and strings are instances of an object

#function
def sample():
	pass

print(f'Function: {isinstance(sample, object)}')

# [SECTION] Parts of an Object
x = 619

print(id(x)) #memory location of this data # Reference count
print(type(x)) #data type of the variable # Type
print(x) #Value

# [SECTION] Two Types of Objects

# IMMUTABLE OBJECTS
numA = 5 # let us call this id id1

print(id(numA))

numA += 1 #Adding 1 to the current value and reassigning it to the variable

print(id(numA)) #id is different from id1
# That is why it is called immutable

# MUTABLE OBJECTS
numbers = [1, 2, 3, 4, 5]

print('-----Before Modifying-----')
print(numbers)
print(id(numbers))

numbers[0] += 1

print('-----After Modifying-----')
print(numbers)
print(id(numbers))

# [SECTION] Lists
# Sublist
hero = ["Sniper", 50, True]
skills = [
	hero, ["sharpnel", "headshot", "take_aim"]
]
print(skills)

# Indexing through sublists
print(skills[0][1])
print(hero[1])

#length
print(len(hero))

#Concatenate - combines the values od two different lists in one single list 
tasks = ["use_skills", "help_civilian", "play_with_friends"]

new_list = hero + tasks
print(new_list)

#Membership -- checks if a certain value exists inside of a specific list
isExisting = "shrapnel" in skills[1]
print(isExisting)

# NOTE: Lists in python are mutable


# [SECTION] Tuples
grades_tuple = (75, 77, 79)
print(grades_tuple)
print(type(grades_tuple))

# Another way of creating a tuple
print(tuple("Python"))

# Nesting Tuples
album_songs = ("Riot!", ("Miracle", "Fencas", "Thats_What_You_Get"))
print(album_songs)

# Indexing with nested tuple
print(album_songs[1][2]);

# Getting index of a value in a tuple
print(album_songs.index("Riot!"))

# [SECTION] Dictionary
player_one = {
	"username": "68connossieur",
	"role": "Mage",
	"level": 99,
	"team": "in_team"
}

print(player_one)

# Changing values using keys
player_one["level"] -= 5
print(player_one)
