#2 singly linked list
class Node:
	def __init__(self, data):
		self.data = data
		self.next = None

#3 
class ColorLinkedList:
	def __init__(self):
		self.head = None

	def append_list(self, data):
		new_node = Node(data)

		
		if self.head is None:
			self.head = new_node 
			return

		last_node = self.head 

		while last_node.next:
			last_node = last_node.next 

		last_node.next = new_node

	def print_list(self):
		current_node = self.head

		while current_node:
			print(current_node.data)
			current_node = current_node.next

	def prepend_list(self, data):
		new_node = Node(data) 
		new_node.next = self.head
		self.head = new_node

	def delete_node(self, key):
		current_node = self.head

		if current_node and current_node.data == key:
			self.head = current_node.next
			current_node = None
			return 

		previous = None 

		while current_node and current_node.data != key:
			previous = current_node 
			current_node = current_node.next 

		if current_node is None:
			return 
		previous.next = current_node.next

		current_node = None

	def search(self, color):
		current_node = self.head
		while current_node:
			if current_node.data == color:
				return "Yes"
			current_node = current_node.next
		return "No"
	def count(self):
		count = 0
		current_node = self.head
		while current_node:
			count += 1
			current_node = current_node.next
		return count


colors_linked_list = ColorLinkedList()

colors_linked_list.prepend_list('Black')
colors_linked_list.prepend_list('Grey')
colors_linked_list.prepend_list('Blue')
colors_linked_list.prepend_list('Green')
colors_linked_list.prepend_list('Brown')

print('Original nodes of the linked list:')
colors_linked_list.print_list()


print('\nInsert the colour "Purple" in front of the linked list:')
colors_linked_list.prepend_list('Purple')
colors_linked_list.print_list()

print('\nDelete the color "Black" from the linked list:')
colors_linked_list.delete_node('Black')
colors_linked_list.print_list()


print('\nDoes the color "Grey" exist in the linked list?')
print(colors_linked_list.search('Grey'))

print('\nDoes the color "Black" exist in the linked list?')
print(colors_linked_list.search('Black'))

print('\nTotal number of nodes in the linked list:')
print(colors_linked_list.count())


