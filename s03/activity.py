class PriorityLane(object):
	def __init__(self):
		self.queue = []

	def enqueue(self, item):
		self.queue.append(item)

	def dequeue(self):
		if not self.is_empty():
			return self.queue.pop(0)
		else:
			return "Queue is empty."

	def is_empty(self):
		return len(self.queue) == 0

	def peek(self):
		if not self.is_empty():
			return self.queue[-1]
		else:
			return "Queue is empty."

	def size(self):
		return len(self.queue)

	def print(self):
		print(self.queue)


#5
#a. Create object "security"

security = PriorityLane()

#b Add to the list
security.enqueue("normal person")
security.enqueue("PWD")
security.enqueue("pregnant")
security.enqueue("senior")

#Print the list
print(f'Original queue list items:')
security.print()

#c Check if the list is empty
print(f'Is the security list empty? {security.is_empty()}\n')

#d Size
print(f'The current size of the security list: {security.size()}\n')

#e Rear item of the list
print(f'Peek of the new list queue item: {security.peek()}\n')

#f Remove the front item
print(f'The item is removed from the security list: {security.dequeue()}\n')

		
