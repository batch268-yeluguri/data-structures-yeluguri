no_of_rows = int(input("Enter the number of rows -> "))
no_of_columns = int(input("Enter the number of columns  -> "))

matrixOne = []
matrixTwo = []
matrixSum = []

print("Input the values for First matrix")
for i in range(no_of_rows):
    row = []
    for j in range(no_of_columns):
        value = float(input(f"matrixOne[{i}][{j}]: "))
        row.append(value)
    matrixOne.append(row)

print("Input the values for Second matrix")
for i in range(no_of_rows):
    row = []
    for j in range(no_of_columns):
        value = float(input(f"matrixTwo[{i}][{j}]: "))
        row.append(value)
    matrixTwo.append(row)

for i in range(no_of_rows):
    row = []
    for j in range(no_of_columns):
        sum_value = matrixOne[i][j] + matrixTwo[i][j]
        row.append(sum_value)
    matrixSum.append(row)

print("***Display Matrix 1***")
for i in range (no_of_rows):
    print(matrixOne[i])

print("***Display Matrix 2***")
for i in range (no_of_rows):
    print(matrixTwo[i])


# 5

print("Sum of Two matrix")
for i in range (no_of_rows):
    print(matrixSum[i])
